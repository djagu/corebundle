jQuery(document).ready(function(){

    var gajexCoreAjaxRequests = [];

    function gajexCoreFoundRequest(gajexCoreAjaxRequests, box, needToAbort)
    {
        if (gajexCoreAjaxRequests.length > 0)
        {
            for (i in gajexCoreAjaxRequests)
            {
                if (gajexCoreAjaxRequests[i].box !== undefined && gajexCoreAjaxRequests[i].box.is(box))
                {
                    if (needToAbort === undefined || needToAbort === true)
                        gajexCoreAjaxRequests[i].request.abort();
                    gajexCoreAjaxRequests.splice(i, 1);
                    break;
                }
            }
        }
    }

    function gajexCoreGetAjaxData(box)
    {
        if (box === undefined)
            return false;

        var ajaxCoreUrl = box.attr('data-url');
        var ajaxCoreMax = box.attr('data-max');
        var ajaxCorePage = box.attr('data-page');
        var ajaxCoreErrorMessage = box.attr('data-error-message');
        if (ajaxCoreErrorMessage === undefined) {
            ajaxCoreErrorMessage = "Loading error.";
        }
        var el = box.find('.core-ajax-data-box-content');
        var i;

        if (ajaxCoreUrl !== undefined && ajaxCoreMax !== undefined && ajaxCorePage !== undefined)
        {
            ajaxCoreMax = parseInt(ajaxCoreMax);
            ajaxCorePage = parseInt(ajaxCorePage);
            ajaxCoreUrl += "?max=" + ajaxCoreMax + "&beginWith=" + parseInt((ajaxCorePage - 1) *  ajaxCoreMax);

            box.find('.core-ajax-data-filter').each(function(index){

                if ($(this).val().length > 0)
                {
                    ajaxCoreUrl += "&" + $(this).attr('name') + "=" + $(this).val();
                }
            });

            el.html("");
            box.find('.core-ajax-data-box-elements-current').text("-");
            box.find('.core-ajax-data-box-elements').text("-");
            box.find('.core-ajax-data-box-loader').removeClass('hide');

            // Verification si une requete n'est pas deja en cours
            gajexCoreFoundRequest(gajexCoreAjaxRequests, box, true);

            var ajaxReq = $.ajax({
                'url': ajaxCoreUrl,
                'type': 'GET',
                'dataType': 'json',
                'success': function (data, status) {
                    el = box.find('.core-ajax-data-box-content');
                    if (data.content !== undefined) {
                        el.html(data.content);

                        box.find('.core-ajax-data-box-elements-current').text(data.count);
                        box.find('.core-ajax-data-box-elements').text(data.total_count);
                        // Gestion des boutons des pages
                        if (data.count == 0 || data.count == ajaxCoreMax || (data.count == data.total_count && data.count < ajaxCoreMax))
                        {
                            box.find('.core-ajax-data-box-previous').addClass('disabled');
                        }
                        else
                            box.find('.core-ajax-data-box-previous').removeClass('disabled');

                        if (data.count == data.total_count)
                        {
                            box.find('.core-ajax-data-box-next').addClass('disabled');
                        }
                        else
                            box.find('.core-ajax-data-box-next').removeClass('disabled');
                    }
                    else {
                        el.html("");
                    }
                    box.find('.core-ajax-data-box-loader').addClass('hide');
                },
                'error': function (data, status, error) {
                    if (status !== "abort") {
                        el = box.find('.core-ajax-data-box-content');
                        if (ajaxCoreErrorMessage !== false)
                            el.html('<div class="text-danger text-center">' + ajaxCoreErrorMessage + '</span>');
                        box.find('.core-ajax-data-box-loader').addClass('hide');
                    }
                },
                'complete': function(){
                    gajexCoreFoundRequest(gajexCoreAjaxRequests, box, false);
                }
            });
            gajexCoreAjaxRequests.push({
                'box': box,
                'request': ajaxReq
            });
        }
    }

    $('.core-ajax-data-filter').on('input', function(e){
        var box = $(this).closest('.core-ajax-data-box');
        box.attr('data-page', 1);
        gajexCoreGetAjaxData(box);
    });

    $('.core-ajax-data-box-next').click(function(){
        if ($(this).hasClass('disabled') === false)
        {
            var box = $(this).closest('.core-ajax-data-box');
            box.attr('data-page', parseInt(box.attr('data-page')) + 1);
            gajexCoreGetAjaxData(box);
        }
    });

    $('.core-ajax-data-box-previous').click(function(){
        if ($(this).hasClass('disabled') === false)
        {
            var box = $(this).closest('.core-ajax-data-box');
            box.attr('data-page', parseInt(box.attr('data-page')) - 1);
            gajexCoreGetAjaxData(box);
        }
    });

    $('.core-ajax-data-box-refresh').click(function(){
        if ($(this).hasClass('disabled') === false)
        {
            var box = $(this).closest('.core-ajax-data-box');
            gajexCoreGetAjaxData(box);
        }
    });

    $('.core-ajax-data-box').each(function(index){
        gajexCoreGetAjaxData($(this));
    });
});