$(document).ready(function(){
    $(".core-autocomplete").each(function()
    {
        var current = $(this);
       $(this).tagit({
           allowSpaces: true,
           allowDuplicates: true,
           caseSensitive: false,
           showAutocompleteOnFocus: true,
           autocomplete: {
               source: function (request, response) {
                   $.ajax({
                       url: current.attr('data-url'),
                       data: { name: request.term },
                       dataType: "json",
                       success: function(data){
                           response($.map(data, function(v, i){
                               return { label: v.name, value: v.name };
                           }));
                       },
                       error: function () {
                           response([]);
                       }
                   });
               }

           }
       });
    });

    $('[data-datepicker]').each(function(){
        var     current = $(this);
        $(this).datepicker({
            dateFormat: current.attr('data-datepicker')
        });
    });
});