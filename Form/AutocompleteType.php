<?php

namespace Gajex\CoreBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Gajex\CoreBundle\Form\DataTransformer\ObjectToStringTransformer;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AutocompleteType extends AbstractType
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new ObjectToStringTransformer($this->container->get('doctrine')->getManager(), $options['class'], $options['property'], $options['allow_add']);
        $builder->addModelTransformer($transformer);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'class',
        ));
        $resolver->setOptional(array('ajax_route', 'ajax_route_parameters'));

        $resolver->setDefaults(array(
            'property'              => 'name',
            'allow_add'             => false,
            'ajax_url'              => ''
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $parameters = (isset($options['ajax_route_parameters'])) ? $options['ajax_route_parameters'] : array();

        if (isset($options['ajax_route']))
            $url = $this->container->get("router")->generate($options['ajax_route'], $parameters);
        else
            $url = $options['ajax_url'];
        $view->vars['ajax_url'] = $url;
    }

    public function getName()
    {
        return "autocomplete";
    }

    public function getParent()
    {
        return "text";
    }
}