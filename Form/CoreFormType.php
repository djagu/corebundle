<?php
	namespace Gajex\CoreBundle\Form;

	use Symfony\Component\DependencyInjection\ContainerInterface;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;

	abstract class CoreFormType extends AbstractType
	{
		protected $dataClass;
		protected $container;

		public function __construct(ContainerInterface $container)
		{
			$this->container = $container;
			$this->dataClass = $this->container->getParameter($this->getEntityName());
		}

		public function setDefaultOptions(OptionsResolverInterface $resolver)
		{
			$resolver->setDefaults(array(
				'data_class'        => $this->dataClass,
			));
		}

		abstract public function getEntityName();
	}