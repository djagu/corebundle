<?php

namespace Gajex\CoreBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class WizardFormType extends AbstractType
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $step = $options['step'];
        $builderName = "buildStep" . $step;
        if (method_exists($this, $builderName))
        {
            $this->$builderName($builder, $options);
        }
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['wizard_step'] = $options['step'];
        $view->vars['wizard_steps'] = $this->getStepNames();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'step'          => 1,
            'validation_groups' => function(FormInterface $form){
                $step = $form->getConfig()->getOption('step');
                if ($step !== null)
                {
                    return array('Step' . $step);
                }
                else
                    return array('Default');
            }
        ));
    }

    /**
     * @return Integer
     *
     * Returns the maximal number of steps
     *
     * Ex:
     * return 5;
     */
    abstract public function getMaxSteps();

    /**
     * @return Array
     *
     * Returns an array with the step names and options
     * Ex:
     * return array(
     *        array('name' => 'Fiche client', 'required' => true)
     * );
     */
    abstract public function getStepNames();
}