<?php
	namespace Gajex\CoreBundle\Form;

	use Symfony\Component\DependencyInjection\ContainerInterface;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolverInterface;

	abstract class BasicFormWithNameType extends AbstractType
	{
		protected $dataClass;

		public function buildForm(FormBuilderInterface $builder, array $options)
		{
			$builder->add('name', 'text', array(
				'label'         => 'Nom',
				'required'      => true,
			));
		}

		public function setDefaultOptions(OptionsResolverInterface $resolver)
		{
			$resolver->setDefaults(array(
				'data_class'        => $this->dataClass,
			));
		}

		public function __construct(ContainerInterface $container)
        {
            $this->dataClass = $container->getParameter($this->getEntityParameterClass());
        }

        /**
         * Return class name of the entity user in the form as the data_class, registered in the parameters
         */
        abstract public function getEntityParameterClass();
	}