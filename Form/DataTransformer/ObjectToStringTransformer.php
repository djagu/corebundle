<?php

namespace Gajex\CoreBundle\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;

class ObjectToStringTransformer implements DataTransformerInterface
{
    protected $em;
    protected $objectClass;
    protected $findBy;
    protected $allowAdd;

    public function __construct(ObjectManager $em, $objectClass, $findBy = "name", $allowAdd)
    {
        $this->em = $em;
        $this->objectClass = $objectClass;
        $this->findBy = $findBy;
        $this->allowAdd = $allowAdd;
    }

    public function transform($collection)
    {
        $string = "";
        if ($collection == NULL)
            return $string;
        foreach($collection as $key => $value)
        {
            if ($key != 0)
                $string .= ',';
            $string .= $value;
        }

        return $string;
    }

    public function reverseTransform($string)
    {
        $collection = new ArrayCollection();

        $objectsArray = explode(',', $string);
        $rp = $this->em->getRepository($this->objectClass);
        foreach ($objectsArray as $item)
        {
            $object = $rp->findAllByFilters(
                array(array('name' => 'entity.' . $this->findBy, 'operator' => 'LIKE', 'value' => $item)), 1
            );
            if ($object === NULL && $this->allowAdd)
            {
                $method = "set" . $this->findBy;
                $object = new $this->objectClass;
                $object->$method($item);
            }
            if ($object == NULL && !$this->allowAdd)
                continue ;
            $collection->add($object);
        }
        return $collection;
    }
}