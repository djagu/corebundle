<?php
	namespace Gajex\CoreBundle\Repository;

	use Doctrine\ORM\EntityRepository;
	use Doctrine\ORM\Query\Expr\Join;
	use Doctrine\ORM\QueryBuilder;

	class CoreRepository extends EntityRepository
	{

		protected $joined;

		public function findAll($max = -1, $orderBy = 'id', $orderByDesc = "ASC")
		{
			$query = $this->createQueryBuilder('entity');
			if ($max != -1 && $max > 0)
			{
				$query->setMaxResults($max);
			}
			$query->orderBy('entity.' . $orderBy, $orderByDesc);
			return $query->getQuery()->getResult();
		}

		public function countAll()
		{
			$query = $this->createQueryBuilder('entity');
			$query->select('COUNT(DISTINCT entity.id)');

			return $query->getQuery()->getSingleScalarResult();
		}


        /**
         * @param array $filters
         * @return mixed
         */
        public function countAllByFilters(array $filters = array())
        {
            $this->joined = array();
            $query = $this->createQueryBuilder('entity');
            $query->select('COUNT(DISTINCT entity.id)');
            $query = $this->joinWithFilters($query, $filters);


            /*
             * Setting filters
             */
            $index  = 0;
            foreach ($filters as $filter)
            {
                $parameters = explode('.', $filter['name']);
                if (count($parameters) == 2)
                {
                    if ($index == 0)
                    {
                        $query->andWhere($filter['name'] . ' ' . $filter['operator'] . ' :filterValue' . $index);
                    }
                    else
                    {
                        $query->andWhere($filter['name'] . ' ' . $filter['operator'] . ' :filterValue' . $index);
                    }
                    $query->setParameter('filterValue'.$index, $filter['value']);
                }
                $index++;
            }

            return $query->getQuery()->getSingleScalarResult();
        }


		/**
		 * @param $query QueryBuilder
		 * @param array $filters
		 * @return mixed
		 */
		protected  function joinWithFilters($query, array $filters)
		{
			foreach ($filters as $key => $filter)
			{
				if ($filter['value'] == null)
				{
					continue ;
				}
				$joinTable = 'entity';
				$parameters = explode('.', $filter['name']);
				if (count($parameters) > 2)
				{
					$completeJoinTable = '';
					for ($i = 1; $i < (count($parameters) - 1); $i++)
					{
						$completeJoinTable =  $joinTable . '.' . $parameters[$i];
						$joinTable = $parameters[$i] . rand(1, 100000);
						if (!array_key_exists($completeJoinTable, $this->joined))
						{
							$query->leftJoin($completeJoinTable, $joinTable);
							$this->joined[$completeJoinTable] = $joinTable;
						}
						else
						{
							$joinTable = $this->joined[$completeJoinTable];
						}
					}

					$joinVar = $parameters[$i];

					if (is_array($filter['value']))
					{
						/*
						 * /!\
						 * To finish
						 * /!\
						 */
						$orCondition = $query->expr()->orX();
						$joinParameters = array();
						foreach ($filter['value'] as $k => $value)
						{
							$orCondition->add($joinTable . '.' . $joinVar . ' ' . $filter['operator'] . ' :joinArrayValue' . $k);
							$joinParameters['joinArrayValue' . $k] = $value;
						}
						$query->andWhere($orCondition);
						$query->setParameters($joinParameters);
					}
					else
					{
						$query->andWhere($joinTable . '.' . $joinVar . ' ' . $filter['operator'] . ' :joinValue' . $key);
						$query->setParameter('joinValue' . $key, $filter['value']);
					}

					unset($filters[$key]);
				}
			}

			return $query;
		}

		/**
		 * @param array $filters
		 * @param $max
		 * @param int $beginWith
		 * @param string $orderBy
		 * @param string $orderByOrder
		 */
		public function findAllByFilters(array $filters, $max = -1, $beginWith = 0, $orderBy = 'id', $orderByOrder = 'ASC')
		{
			$this->joined = array();
			$query = $this->createQueryBuilder('entity');
			$query = $this->joinWithFilters($query, $filters);

			/*
			 * Setting filters
			 */
			$index  = 0;
			foreach ($filters as $filter)
			{
				$parameters = explode('.', $filter['name']);
				if (count($parameters) == 2)
				{
					if ($index == 0)
					{
						$query->andWhere($filter['name'] . ' ' . $filter['operator'] . ' :filterValue' . $index);
					}
					else
					{
						$query->andWhere($filter['name'] . ' ' . $filter['operator'] . ' :filterValue' . $index);
					}
					$query->setParameter('filterValue'.$index, $filter['value']);
				}
				$index++;
			}

			/*
			 * Setting parameters
			 */
			if ($max != -1 && $max > 0)
			{
				$query->setMaxResults(intval($max));
			}
			$query->setFirstResult(intval($beginWith));
			if (in_array(strtolower($orderByOrder), array('asc', 'desc')) && preg_match_all('#[a-zA-Z]+#', $orderBy, $match))
			{
				$query->orderBy('entity.' . $orderBy, $orderByOrder);
			}

			if ($max == 1)
			{
				return $query->getQuery()->getOneOrNullResult();
			}

			return $query->getQuery()->getResult();
		}
	}
