<?php

namespace Gajex\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\Serializer\SerializationContext;
use Gajex\CoreBundle\CoreEvents;
use Gajex\CoreBundle\Event;
use Symfony\Component\Form\FormError;


abstract class CoreController extends Controller
{
	protected $parameters;
	protected $dispatcher;

    const SPECIAL_TYPE_INT = 0;
    const SPECIAL_TYPE_STRING = 1;

    public function __construct()
    {
        $this->parameters = array();
        $this->parameter['form'] = array();
        $this->parameter['render'] = array();
	    $this->parameters['render']['base_route'] = $this->getBaseRoute();
	    $this->parameters['render']['ajaxRequest'] = false;
    }

	public function setAjax(Request $request)
	{
		if ($request->isXmlHttpRequest())
		{
			$this->parameters['render']['ajaxRequest'] = true;
		}
	}

    public function createEntity($id = null)
    {
        $entityClass = $this->container->getParameter($this->getEntityName());
        if ($id !== null)
        {
            $rp = $this->getDoctrine()->getManager()->getRepository($entityClass);
            $entity = $rp->find($id);
            if (!$entity)
            {
                throw new NotFoundHttpException("Entity does not exist");
            }
        }
        else
        {
            $entity = new $entityClass;
        }

        return $entity;
    }

	public function createAction(Request $request)
	{
		$entity = $this->createEntity();

		return $this->getForm($entity, 'create', $request);
	}

	public function editAction($id, Request $request)
	{
		$entity = $this->createEntity($id);

		return $this->getForm($entity, 'edit', $request);
	}

    public function wizardAction($step = 1, $id = null, Request $request)
    {
        $this->parameters['form']['step'] = $step;
        $form = new $this->getFormType();
        $controller = $this;
        $this->parameters['global']['render_template'] = $this->getBasePath() . '/Wizard:step' . $step . '.html.twig';


        // Calling custom wizard method if defined in the child controller
        $methodName = "wizardStep" . $step;
        if (method_exists($this, $methodName))
        {
            $this->$methodName($step, $id, $request);
        }
        /*
         * Edit action
         */
        if ($id !== null)
        {
            $entity = $this->createEntity($id);
            $params = array('step' => $step, 'id' => $id);
            $this->parameters['form']['action'] = $this->generateUrl($this->getBaseRoute() . '_' . 'wizard', $params);
            $this->get('event_dispatcher')->addListener(CoreEvents::CORE_POST . "edit", function(Event\CoreEvent $event) use ($controller, $step, $form){
                $id = $event->getSubject()->getId();
                // Redirecting to the next step only if it exists
                if (($step + 1) <= $form->getMaxSteps())
                {
                    $event->setArgument('redirection_url', $controller->generateUrl($this->getBaseRoute() . "_wizard", array('step' => $step + 1, 'id' => $id)));
                }
            });

            return $this->getForm($entity, 'edit', $request);
        }
        else
        {
            /*
             * Create action
             */
            $entity = $this->createEntity();
            $params = array('step' => $step);
            $this->parameters['form']['action'] = $this->generateUrl($this->getBaseRoute() . '_' . 'wizard', $params);
            $this->get('event_dispatcher')->addListener(CoreEvents::CORE_POST . "create", function(Event\CoreEvent $event) use ($controller, $step, $form){
                $id = $event->getSubject()->getId();
                // Redirecting to the next step only if it exists
                if (($step + 1) <= $form->getMaxSteps())
                    $event->setArgument('redirection_url', $controller->generateUrl($this->getBaseRoute() . "_wizard", array('step' => $step + 1, 'id' => $id)));
            });
            $this->parameters['form']['validation_groups'] = array('Step' . $step);
            return $this->getForm($entity, 'create', $request);
        }
    }

	public function deleteAction($id)
	{
		$entity = $this->createEntity($id);

		$em = $this->getDoctrine()->getManager();
		$em->remove($entity);
		$em->flush();

		$this->container->get('session')->getFlashBag()->add('success', 'The entity has been removed !');
        if (!isset($this->parameters['render']['redirection_url']))
		    return $this->redirect($this->generateUrl($this->getBaseRoute() . '_' . 'index'));
        return $this->redirect($this->parameters['render']['redirection_url']);
    }

	public function indexAction($page, $perPage = 10)
	{
		$rp = $this->getDoctrine()->getManager()->getRepository($this->container->getParameter($this->getEntityName()));
		$entities = $rp->findAllByFilters(array(), $perPage, ($page - 1) * $perPage, 'id', 'DESC');
		$entityCount = $rp->countAll();
		$pages = intval($entityCount / $perPage);
		$pages = ($entityCount % $perPage) ? $pages + 1 : $pages;

		$this->parameters['render']['perPage'] = $perPage;
		$this->parameters['render']['currentPage'] = $page;
		$this->parameters['render']['maxPages'] = $pages;
		$this->parameters['render']['entities'] = $entities;
		$this->parameters['render']['action'] = 'index';
		$this->parameters['render']['entityCount'] = $entityCount;

		return $this->render($this->getBasePath() . ':index.html.twig', $this->parameters['render']);
	}

	public function getForm($entity, $action, Request $request)
	{
        if (!isset($this->parameters['form']['action']))
        {
            if ($action == 'edit') {
                $formAction = $this->generateUrl($this->getBaseRoute() . '_' . $action, array('id' => $entity->getId()));
            } else {
                $formAction = $this->generateUrl($this->getBaseRoute() . '_' . $action);
            }
            $this->parameters['form']['action'] = $formAction;
        }
		$this->parameters['form']['method'] = 'POST';

		$form = $this->createForm($this->getFormType(), $entity, $this->parameters['form']);

		/*
		 * Dispatching custom event (PRE_CREATE/EDIT or other)
		 */
		$this->dispatcher = $this->get('event_dispatcher');
		$event = new Event\CoreEvent($entity, array('form' => $form, 'request' => $request, 'parameters' => array()));
		$this->dispatcher->dispatch(CoreEvents::CORE_PRE . $action, $event);

		/*
		 * Handling the form request
		 */
		$em = $this->getDoctrine()->getManager();
		if ($request->isMethod('post'))
		{
			$form->handleRequest($request);
			if ($form->isValid())
			{
                $error = null;
                $fieldError = "";
				/*
				 * Dispatching post bind data event, so we can access to the hole validated data
				 */
				$postBindEvent = new Event\CoreEvent($entity, array('form' => $form, 'request' => $request, 'flush' => true, 'error' => '', 'redirection_url' => '', 'field_error' => '', 'parameters' => array()));
				$this->dispatcher->dispatch(CoreEvents::CORE_POST_BIND, $postBindEvent);
                $rurl = $postBindEvent->getArgument('redirection_url');
                if (!empty($rurl))
                {
                    $this->parameters['render']['redirection_url'] = $rurl;
                }
				if ($postBindEvent->getArgument('flush') === true)
				{
					$em->persist($entity);
					$em->flush();

                    /*
                     * Dispatching custom event (POST_CREATE/EDIT or other)
                     */
                    $event = new Event\CoreEvent($postBindEvent->getSubject(), array('form' => $form, 'request' => $request, 'redirection_url' => '', 'parameters' => $postBindEvent->getArgument('parameters')));
                    $this->dispatcher->dispatch(CoreEvents::CORE_POST . $action, $event);
                    $rurl = $event->getArgument('redirection_url');
                    if (!empty($rurl))
                    {
                        $this->parameters['render']['redirection_url'] = $rurl;
                    }
                    if ($request->isXmlHttpRequest())
                    {
                        return new Response();
                    }
				}
                else
                {
                    // Giving the oportunity to set an error, and to redirect to a
                    $error = $postBindEvent->getArgument('error');
                    $fieldError = $postBindEvent->getArgument('field_error');;
                    if (!empty($fieldError))
                    {
                        $form->get($fieldError)->addError(new FormError($error));
                    }
                }



                if ($error != NULL)
                {
                    $this->container->get('session')->getFlashBag()->add('success', 'Changement effectué avec succès !');
                }
                else
                {
                    if ($action == 'create')
                    {
                        $this->container->get('session')->getFlashBag()->add('success', 'Création effectuée avec succès !');
                    }
                    else if ($action == 'edit')
                    {
                        $this->container->get('session')->getFlashBag()->add('success', 'Changement effectué avec succès !');
                    }
                }
                // Redirecting only if an other error in the form has not been set
                if (empty($fieldError))
                {
                    if (!isset($this->parameters['render']['redirection_url']))
                        return $this->redirect($this->generateUrl($this->getBaseRoute() . '_' . 'index'));
                    return $this->redirect($this->parameters['render']['redirection_url']);
                }
			}
		}

		$this->parameters['render']['form'] = $form->createView();
		$this->parameters['render']['entity'] = $event->getSubject();
		$this->parameters['render']['action'] = $action;

		$this->setAjax($request);

        if (!isset($this->parameters['global']['render_template']))
            $render_template = $this->getBasePath() . ':' . $action . '.html.twig';
        else
            $render_template = $this->parameters['global']['render_template'];

		return $this->render($render_template, $this->parameters['render']);
	}

	public function getEntityClass()
	{
		return $this->container->getParameter($this->getEntityName());
	}

	public function listAction($perPage)
	{
		$perPage = ($perPage > 100 || $perPage < 0) ? 100 : $perPage;

		$entities = $this->getDoctrine()->getManager()->getRepository($this->getEntityClass())->findAll($perPage);

		return $this->createJsonResponse($entities, 200);
	}

	/*************************************************************
	 * Handling Json
	 *************************************************************/

	private function isParametersArray($string)
	{

	}

	protected function getFilterParametersFromQuery(Request $request)
	{
		$filters = array();
		$filterParameters = array(
			'max'               => -1,
			'orderBy'           => 'id',
			'orderByOrder'      => 'ASC',
			'beginWith'         => 0,
		);
		$params = $request->query->all();

		foreach ($params as $key => $value)
		{
			/*
			 * Checking if the parameter is a special filter parameter
			 */
			if (array_key_exists($key, $filterParameters))
			{
				$filterParameters[$key] = $value;
				continue ;
			}
			$parameter = array();
			$parameter['name'] = str_replace('_', '.', $key);
			$matches = array();

            if (preg_match_all('#^([a-zA-Z]+(\.)?)+$#', $parameter['name'], $matches) == 0)
            {
                return null;
            }

            // Handling special types
            $specialType = NULL;
            if (substr($value, 0, strlen("[int]")) === "[int]")
            {
                $len = strlen("[int]");
                $value = substr($value, strlen("[int]"), strlen($value) - $len);
                $specialType = self::SPECIAL_TYPE_INT;
            }
            else
            {
                $len = strlen("[string]");
                $value = substr($value, strlen("[string]"), strlen($value) - $len);
                $specialType = self::SPECIAL_TYPE_STRING;
            }


			if ($specialType === self::SPECIAL_TYPE_INT) {
                $parameter['operator'] = '=';
                $parameter['value'] = intval($this->getFilterValueFromQuery($value));
            }
            else
            {
                $parameter['operator'] = 'LIKE';
                $parameter['value'] = $this->getFilterValueFromQuery($value);
            }

			$filters[] = $parameter;
		}

		return array(
			'filters'           => $this->preFilter($filters),
			'parameters'        => $filterParameters
		);
	}

	protected function getFilterValueFromQuery($value)
	{
		$values = explode(',', $value);
		if (count($values) > 1)
		{
			return $values;
		}

		return $value;
	}

	protected function preFilter($filters)
	{
		foreach ($filters as &$filter)
		{
			if ($filter['operator'] === "LIKE")
			{
				if (!empty($filter['value']))
				{
					$filter['value'] = '%' . $filter['value'] .'%';
				}
			}

			$filter['name'] = 'entity.' . $filter['name'];
		}

		return $filters;
	}

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Request $request)
    {
        if ($request->isXmlHttpRequest())
        {
            // Sending the AJAX content
            $response = new JsonResponse();
            $data = array();

            $queryParameters = $this->getFilterParametersFromQuery($request);
            $entities = $this->getEntitiesWithParameters(null, $queryParameters);
            $data['success'] = true;
            if (isset($this->parameters['custom__view']) && !empty($this->parameters['custom__view']))
                $view = $this->parameters['custom__view'];
            else
                $view = "_view.html.twig";

            $data['content'] = $this->get('templating')->render($this->getBasePath() . ':' . $view, array(
                'entities'      => $entities
            ));
            if ($queryParameters)
            {
                $data['count'] = 0;
                if ($queryParameters['parameters']['beginWith'] > 0)
                {
                    $data['count'] += intval($queryParameters['parameters']['beginWith']);
                }
                if ($queryParameters['parameters']['max'] > 0)
                {
                    $data['count'] += $queryParameters['parameters']['max'];
                }
                $data['total_count'] = $this->getDoctrine()->getManager()->getRepository($this->getEntityClass())->countAllByFilters($queryParameters['filters']);
                if (($data['count'] > $data['total_count']) || ($data['count'] == 0 && $data['total_count'] != 0))
                {
                    $data['count'] = $data['total_count'];
                }
            }
            else
            {
                $data['count'] = 0;
                $data['total_count'] = 0;
            }


            $response->setData($data);
            return $response;
        }
        else
        {
            // Sending the base page
            if (isset($this->parameters['custom_view']) && !empty($this->parameters['custom_view']))
            {
                $view = $this->parameters['custom_view'];
            }
            else
                $view = "view.html.twig";
            return $this->render($this->getBasePath() . ":" . $view, array(
                'base_route'            => $this->getBaseRoute(),
                'parameters'            => $this->parameters['render']
            ));
        }
    }

    public function getEntitiesWithParameters($id, $queryParameters)
    {
        $entity = null;
        if ($queryParameters)
        {
            /*
             * Checking if the id is set
             */
            if ($id !== null)
            {
                $queryParameters['parameters']['max'] = 1;
                $queryParameters['filters'][] = array(
                    'name'          => 'entity.id',
                    'operator'      => '=',
                    'value'         => $id
                );
            }
            $entity = $this->getDoctrine()->getManager()->getRepository($this->getEntityClass())->findAllByFilters(
                $queryParameters['filters'],
                $queryParameters['parameters']['max'],
                $queryParameters['parameters']['beginWith'],
                $queryParameters['parameters']['orderBy'],
                $queryParameters['parameters']['orderByOrder']
            );
        }

        return $entity;
    }


	public function getAction($id = null, Request $request)
	{
        $queryParameters = $this->getFilterParametersFromQuery($request);
		$entity = $this->getEntitiesWithParameters($id, $queryParameters);

		if ($entity)
		{
			return $this->createJsonResponse($entity, 200);
		}

		return $this->createJsonResponse(array(), 200);
	}

	public function createJsonResponse($data, $code = 200)
	{
		$serializer = $this->container->get('jms_serializer');
		$dataJson = $serializer->serialize($data, 'json', SerializationContext::create()->enableMaxDepthChecks());


		$response = new Response($dataJson, $code);
		$response->headers->set('Content-Type', 'application/json');


		return $response;
	}
	/*************************************************************
	 * End handling json
	 *************************************************************/

	/**
	 * @return mixed
	 *
	 * Return the base of the routes
	 * ex: my_project_superbundle
	 */
	abstract public function getBaseRoute();

	/**
	 * @return mixed
	 *
	 * Return the base path of the views
	 * ex: MyProjectSuperBundle:MyViewDir
	 */
	abstract public function getBasePath();

	/**
	 * @return mixed
	 *
	 * Return the name of the registred for type in the services
	 */
	abstract public function getFormType();

	/**
	 * @return mixed
	 *
	 * Return the name of the entity registred in the parameters
	 */
	abstract public function getEntityName();
}
