<?php
	namespace Gajex\CoreBundle\Listener;

	use Doctrine\ORM\Event\LifecycleEventArgs;
	use Gajex\CoreBundle\Model\BasicEntity;
	use Symfony\Component\DependencyInjection\ContainerInterface;
	use Symfony\Component\Validator\Constraints\DateTime;

	class BasicEntityListener extends BasicListener
	{
		public function prePersist(LifecycleEventArgs $args)
		{
			$entity = $args->getEntity();
			if ($entity instanceof BasicEntity)
			{
				$entity->setCreatedAt(new \DateTime());
                if ($entity->getOwner() == "" || $entity->getOwner() == NULL)
                {
                    $token = $this->container->get('security.token_storage')->getToken();
                    if($token != NULL && is_object($token->getUser()))
                        $entity->setOwner($token->getUser());
                    else
                        $entity->setOwner(NULL);
                }
			}
		}
	}
