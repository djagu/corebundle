<?php
	namespace Gajex\CoreBundle\Listener;

	use Symfony\Component\DependencyInjection\ContainerInterface;

	abstract class BasicListener
	{
		protected $container;
		protected $em;
		protected $dataClass;

		public function __construct(ContainerInterface $container)
		{
			$this->container = $container;
		}
	}