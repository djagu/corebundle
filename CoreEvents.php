<?php
	namespace Gajex\CoreBundle;

	final class CoreEvents
	{
		const CORE_PRE = 'gajex_core.pre_';


		const CORE_POST = 'gajex_core.post_';

		const CORE_POST_BIND = 'gajex_core.post_bind';
	}