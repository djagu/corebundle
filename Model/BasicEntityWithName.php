<?php
	namespace Gajex\CoreBundle\Model;

	use Doctrine\ORM\Mapping as ORM;

	/**
	 * Class BasicEntityWithName
	 * @package Gajex\CoreBundle\Model
	 *
	 * @ORM\MappedSuperclass()
	 */
	class BasicEntityWithName extends BasicEntity
	{
		/**
		 * @var string
		 *
		 * @ORM\Column(name="name", type="string", length=255)
		 */
		protected $name;

		public function __construct()
		{
			parent::__construct();
			$this->name = "";
		}

		public function __toString()
		{
			return $this->name;
		}

		/**
		 * Set name
		 *
		 * @param string $name
		 * @return BasicEntityWithName
		 */
		public function setName($name)
		{
			$this->name = $name;

			return $this;
		}

		/**
		 * Get name
		 *
		 * @return string
		 */
		public function getName()
		{
			return $this->name;
		}
	}