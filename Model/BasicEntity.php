<?php

namespace Gajex\CoreBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use UserBundle\Entity\User;

/**
 * BasicEntity
 * @ORM\MappedSuperclass()
 *
 * @Serializer\ExclusionPolicy("all")
 */
class BasicEntity
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 *
	 * @Serializer\Expose
	 */
	protected $id;

	/**
	 * @var datetime
	 *
	 * @ORM\Column(type="datetime", nullable=false)
	 * @Serializer\Expose
	 */
	protected $createdAt;

	/**
	 * @var datetime
	 *
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $updatedAt;

	/**
	 * @var \Olysogub\UserBundle\Entity\User
	 *
	 * @Assert\Valid()
	 * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", cascade="persist")
     * @ORM\JoinColumn(nullable=true)
	 */
	protected $owner;

	/**
	 * @var \Olysogub\UserBundle\Entity\User
	 *
	 * @Assert\Valid()
	 * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", cascade="persist")
	 */
	protected $updateUser;

	public function __construct()
	{
		$this->createdAt = new \DateTime();
	}

	public function __toString()
	{
		return "Undefined string for the class";
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	public function setCreatedAt($date)
	{
		$this->createdAt = $date;

		return $this;
	}

	public function getUpdatedAt()
	{
		return $this->updatedAt;
	}

	public function setUpdatedAt($date)
	{
		$this->updatedAt = $date;

		return $this;
	}

	public function getOwner()
	{
		return $this->owner;
	}

	public function setOwner($owner)
	{
        if (!($owner instanceof User))
        {
            $owner = NULL;
        }
		$this->owner = $owner;

		return $this;
	}

	public function getUpdateUser()
	{
		return $this->updateUser;
	}

	public function setUpdateUser($user)
	{
		$this->updateUser = $user;

		return $this;
	}
}
