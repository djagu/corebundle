<?php
	namespace Gajex\CoreBundle\Model;

	use Gajex\CoreBundle\Model\BasicEntityWithName;
	use Doctrine\ORM\Mapping as ORM;
	use Gedmo\Mapping\Annotation as Gedmo;


	/**
	 * Class BasicEntityWithSlug
	 * @package Gajex\CoreBundle\Model
	 *
	 * @ORM\MappedSuperclass
	 */
	class BasicEntityWithSlug extends BasicEntityWithName
	{
		/**
		 * @var string
		 *
		 * @Gedmo\Slug(fields={"name"})
		 * @ORM\Column(name="slug", type="string", length=255)
		 */
		protected  $slug;


		/**
		 * Set slug
		 *
		 * @param string $slug
		 * @return User
		 */
		public function setSlug($slug)
		{
			$this->slug = $slug;

			return $this;
		}

		/**
		 * Get slug
		 *
		 * @return string
		 */
		public function getSlug()
		{
			return $this->slug;
		}
	}