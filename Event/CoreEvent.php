<?php
	namespace Gajex\CoreBundle\Event;

	use Symfony\Component\EventDispatcher\GenericEvent;

	class CoreEvent extends GenericEvent
	{
		public function setSubject($subject)
		{
			$this->subject = $subject;

			return $this;
		}
	}